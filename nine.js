const fs = require("fs");

// Problem statement:- 

// 1) create file after 1 second 
// 2) Read file after 2 sec 
// 3) Delete file after 3sec

const readFile = (filename, cb) => {
  fs.readFile(filename, "utf8", (err, data) => {
    if (err) {
      cb(err);
    } else {
      cb(null, data);
    }
  });
};

const writeFile = (filename, data, cb) => {
  fs.writeFile(filename, data, (err, data) => {
    if (err) {
      cb(err);
    } else {
      cb(null,data);
    }
  });
};

const deleteFile = ((filename, cb) => {
  fs.unlink(filename, (err, data) => {
    if(err) {
      cb(err);
    }else {
      cb(null, data);
    }
  })
})

writeFile('a.txt', 'hello', (err, data) => {
    if(err) {
        console.log(err);
    }else {
        console.log('Sucessfully Write the file');
        readFile('a.txt', (err, data) => {
            if (err) {
                console.log(err);
            }else {
                console.log(data);
                deleteFile('a.txt', (err, data) => {
                    if(err) {
                        console.log(err);
                    }else {
                        console.log("Successfully Delete the file");
                    }
                })
            }
        })
    }
})

const runFile = ((n, cb) => {
    setTimeout(() => {
        cb(null, `${n}sec`);
    }, n*1000)
})


runFile(1, (err, data) => {
    if(err) {
        console.log("Line 70 : error");
    }else {
        console.log(data);
        writeFile('a.txt', 'hello', (err, data) => {
            if(err) {
                console.log(err);
            }else {
                console.log("Successfully Write the file");
                runFile(2, (err, data) => {
                    if(err) {
                        console.log("Line 80 : error");
                    }else {
                        console.log(data);
                        readFile('a.txt', (err, data) => {
                            if(err) {
                                console.log(err);
                            }else {
                                console.log("Successfully read the file");
                                runFile(3, (err, data) => {
                                    if(err) {
                                        console.log('Line 90 : Error');
                                    }else {
                                        console.log(data);
                                        deleteFile('a.txt', (err, data) => {
                                            if(err) {
                                                console.log(err);
                                            }else {
                                                console.log("Successfully delete the file");
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
})

