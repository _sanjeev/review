const fs = require ('fs');

const readFile = (filename,  cb) => {
    // fs.writeFile(filename, data, (err, data) => {
    //     if (err) {
    //         cb (err);
    //     }else {
    //         cb (null, data);
    //     }
    // })
    // fs.rename (filename, data, (err, data) => {
    //     if (err) {
    //         cb (err);
    //     }else {
    //         cb (null, data);
    //     }
    // })
    fs.readFile(filename, 'utf8', (err, data) => {
        if (err) {
            cb (err);
        }else {
            cb (null, data);
        }
    })
}

const writeFile = (filename, data, cb) => {
    fs.writeFile(filename, data, (err, data) => {
        if (err) {
            cb (err);
        }else {
            cb (null, data);
        }
    })
}

// setTimeout(() => {}, 1000);

// readFile ('new.txt', (err, data) => {
//     if (err) {
//         console.log(err);
//     }else {
//         const updata = data.toUpperCase();
//         readFile('b.txt', (err, data) => {
//             if(err) {
//                 console.log(err);
//             }else {
//                 const lodata = data.toLowerCase();
//                 let str = updata + lodata;
//                 console.log(str);
//             }
//         })
//     }
// })

writeFile('a.txt', 'hello', (err, data) => {
    if (err) {
        console.log(err);
    }else {
        setTimeout(() => {
            readFile ('a.txt', (err, data) => {
                if (err) {
                    console.log(err);
                }else {
                    console.log(data);
                }
            })
        }, 1000)
    }
})