let obj = [{ "id": 1, "first_name": "Valera", "last_name": "Pinsent", "email": "vpinsent0@google.co.jp", "gender": "Male", "ip_address": "253.171.63.171" },
{ "id": 2, "first_name": "Kenneth", "last_name": "Hinemoor", "email": "khinemoor1@yellowbook.com", "gender": "Polygender", "ip_address": "50.231.58.150" },
{ "id": 3, "first_name": "Roman", "last_name": "Sedcole", "email": "rsedcole2@addtoany.com", "gender": "Genderqueer", "ip_address": "236.52.184.83" },
{ "id": 4, "first_name": "Lind", "last_name": "Ladyman", "email": "lladyman3@wordpress.org", "gender": "Male", "ip_address": "118.12.213.144" },
{ "id": 5, "first_name": "Jocelyne", "last_name": "Casse", "email": "jcasse4@ehow.com", "gender": "Agender", "ip_address": "176.202.254.113" },
{ "id": 6, "first_name": "Stafford", "last_name": "Dandy", "email": "sdandy5@exblog.jp", "gender": "Female", "ip_address": "111.139.161.143" },
{ "id": 7, "first_name": "Jeramey", "last_name": "Sweetsur", "email": "jsweetsur6@youtube.com", "gender": "Genderqueer", "ip_address": "196.247.246.106" },
{ "id": 8, "first_name": "Anna-diane", "last_name": "Wingar", "email": "awingar7@auda.org.au", "gender": "Agender", "ip_address": "148.229.65.98" },
{ "id": 9, "first_name": "Cherianne", "last_name": "Rantoul", "email": "crantoul8@craigslist.org", "gender": "Genderfluid", "ip_address": "141.40.134.234" },
{ "id": 10, "first_name": "Nico", "last_name": "Dunstall", "email": "ndunstall9@technorati.com", "gender": "Female", "ip_address": "37.12.213.144" }]

// 1. Find all people who are Agender

const data = obj.filter((val) => {
    return val.gender === 'Agender';
})


// 2. Split their IP address into their components eg. 111.139.161.143 has components 111 139 161 143.

const res1 = obj.map((val) => {
    let ip = val.ip_address;
    let ip_add = ip.split('.').join(' ');
    val.ip_address = ip_add;
    return val;
})

// 3. Find the sum of all the second components of the ip addresses.

const res2 = obj.reduce((acc, val) => {
    let add = val.ip_address.split(' ');
    acc += Number(add[0]) + Number(add[1]) + Number(add[2]) + Number(add[3]);
    return acc;
}, 0);

// 3. Find the sum of all the fourth components of the ip addresses.

const res3 = obj.reduce((acc, val) => {
    let add = val.ip_address.split(' ')[1];
    acc += Number(add);
    return acc;
}, 0);

// 4. Compute the full name of each person

const res4 = obj.map((val) => {
    val.fullName = val.first_name + ' ' + val.last_name;
    // delete val.last_name;
    // delete val.first_name;
    return val;
})

// console.log(res4);

// 5. Filter out all the .org emails

const res5 = obj.filter((val) => {
    return val.email.substring(val.email.length - 3) === 'org';
})
// console.log(res5);
// 6. Calculate how many .org, .au, .com emails are there

const res6 = obj.reduce((acc, val) => {
    if (val.email.includes('.com')) {
        if (!acc['.com']) {
            acc['.com'] = 1;
        } else {
            acc['.com']++;
        }
    }
    else if (val.email.includes('.au')) {
        // console.log(val.email.includes('.au'));
        if (!acc['.au']) {
            acc['.au'] = 1;
        } else {
            acc['.au']++;
        }
    }
    else if (val.email.includes('.org')) {
        if (!acc['.org']) {
            acc['.org'] = 1;
        } else {
            acc['.org']++;
        }
    }
    return acc;
}, {})

// console.log(res6);

// 7. Sort the data in descending order of first name

const res7 = obj.sort((a, b) => {
    if (a.first_name < b.first_name) {
        return -1;
    }
    if (a.first_name > b.first_name) {
        return 1;
    }
    return -1;
})

console.log(res7);