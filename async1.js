const fs = require('fs');

const writeFile = ((filename, data, cb) => {
    fs.writeFile(filename, data, 'utf8', (err, data) => {
        if (err) {
            cb(err);
        }else {
            cb (null, data);
        }
    })
})

const readFile = ((filename, cb) => {
    fs.readFile(filename, 'utf8', (err, data) => {
        if(err) {
            cb(err);
        }else {
            cb (null, data);
        }
    })
})

writeFile ('countries.txt', 'India, USA', (err, data) => {
    if(err) {
        console.log(err);
    }else {
        console.log(data);
        writeFile('state.txt', 'Karnataka, Tamil Nadu', (err, data) => {
            if (err) {
                console.log(err);
            }else {
                console.log(data);
                readFile ('state.txt', (err, stateData) => {
                    if(err) {
                        console.log(err);
                    }else {
                        console.log(stateData.toUpperCase());
                        readFile('countries.txt', (err, countriesData) => {
                            if(err) {
                                console.log(err);
                            }else {
                                console.log(countriesData.toLowerCase());
                                writeFile('combined.txt', countriesData.toLowerCase() + " " + stateData.toUpperCase(), (err, data) => {
                                    if(err) {
                                        console.log(err);
                                    }else {
                                        console.log(data);
                                        readFile('combined.txt', (err, data) => {
                                            if(err) {
                                                console.log(err);
                                            }else {
                                                console.log(data);
                                                const res = data.split(' ').join(' ');

                                                console.log(res);
                                                readFile('data.json', (err, data) => {
                                                    if(err) {
                                                        console.log(err);
                                                    }else {
                                                        console.log(data);
                                                        const arr = ['state.txt', 'combined.txt', 'data.json'];
                                                        arr.forEach((key) => {
                                                            readFile(key, (err, data) => {
                                                                if(err) {
                                                                    console.log(err);
                                                                }else {
                                                                    console.log(data);
                                                                }
                                                            })
                                                        });
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
})